package com.joaoricardi.providerandbrodcast

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import androidx.room.Room
import com.joaoricardi.providerandbrodcast.persistence.RV.UserRecyvlerAdapter
import com.joaoricardi.providerandbrodcast.persistence.db.AppDatabase
import com.joaoricardi.providerandbrodcast.persistence.entity.TesteEntity

class MainActivity : AppCompatActivity() {

    lateinit var database: AppDatabase
    var adapter: UserRecyvlerAdapter = UserRecyvlerAdapter()
    var count = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        database = Room.databaseBuilder(
            applicationContext,
            AppDatabase::class.java, "database-name"
        ).build()


        database.testeDAO().getAll().observeForever {
            adapter.items = it
        }

        val btn = findViewById<Button>(R.id.addBtn)
        btn.setOnClickListener {

            count += 1
            database.testeDAO().insertAll(
                TesteEntity(
                    firstName = "TESTE $count",
                    lastName = count.toString(),
                    uid = null
                )
            )

            database.testeDAO().getAll()
        }
    }
}