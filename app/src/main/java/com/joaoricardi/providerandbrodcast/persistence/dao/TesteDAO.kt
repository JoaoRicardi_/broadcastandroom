package com.joaoricardi.providerandbrodcast.persistence.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import com.joaoricardi.providerandbrodcast.persistence.entity.TesteEntity

@Dao
interface TesteDAO {
    @Query("SELECT * FROM TesteEntity")
    fun getAll(): LiveData<List<TesteEntity>>

    @Insert
    fun insertAll(testeEntity: TesteEntity)

    @Delete
    fun delete(testeEntity: TesteEntity)
}