package com.joaoricardi.providerandbrodcast.persistence.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.joaoricardi.providerandbrodcast.persistence.dao.TesteDAO
import com.joaoricardi.providerandbrodcast.persistence.entity.TesteEntity

@Database(entities = arrayOf(TesteEntity::class), version = 1)
abstract class AppDatabase : RoomDatabase() {

    abstract fun testeDAO(): TesteDAO
}