package com.joaoricardi.providerandbrodcast.persistence.RV

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.joaoricardi.providerandbrodcast.R
import com.joaoricardi.providerandbrodcast.persistence.entity.TesteEntity
import kotlin.properties.Delegates

class UserRecyvlerAdapter: RecyclerView.Adapter<UserRecyvlerAdapter.ViewHolder>(){

    var items: List<TesteEntity> by Delegates.observable(emptyList()) { _, _, _ ->
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var v = LayoutInflater.from(parent.context).inflate(R.layout.usr_item, parent, false)

        return ViewHolder(v)
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        var item = items[position]
        holder.bind(item)
    }

    class ViewHolder(v: View): RecyclerView.ViewHolder(v) {

        fun bind(entity: TesteEntity){
            itemView.findViewById<TextView>(R.id.tvId).text = entity.uid.toString()
            itemView.findViewById<TextView>(R.id.tvNameId).text = entity.firstName
        }
    }

}